let aawgConfig;
let customer;
let optional;
let tokenServiceConfig;
let workRequest;
let work;
let audioInteraction;
let active = false;

let timerCall;
let timerPassed = 0;

const AVAYAFUNCTIONS = {

    start: function(){
        if(active){
            return;
        }
        AVAYAFUNCTIONS.initialise();
        AVAYAFUNCTIONS.getToken(AVAYAFUNCTIONS.createInteraction);
    },

    initialise: function () {
        let clientInstance;

        if (aawgConfig) {
            if (!clientInstance) {
                var config = {
                    "webGatewayAddress": aawgConfig.webGatewayAddress,
                    "webGatewayConfiguration": aawgConfig.webGatewayConfiguration,
                    "port": aawgConfig.port,
                    "secure": aawgConfig.secure
                };
                clientInstance = new OceanaCustomerWebVoiceVideo(config);
                console.debug('SDK: version - ' + clientInstance.getVersionNumber());
                console.info('Reference Client: Oceana SDK instance created');
                clientInstance.registerLogger(console);
                console.info('Reference Client: Oceana SDK logger registered');
            } else {
                clientInstance = undefined;
                console.log('SDK: invalid configuration object provided');
            }
        }

        console.log("clientInstance: " + JSON.stringify(clientInstance));

        if (workRequest) {
            work = clientInstance.createWork();
            work.setRoutingStrategy(workRequest.strategy);
            work.setLocale(workRequest.locale);

            console.log(work);

            let service = new OceanaCustomerWebVoiceVideo.Services.Work.Schema.Service();
            service.setPriority(5);
            console.log(service);
            work.setServices([service]);

            if (workRequest.nativeResourceId && workRequest.sourceName) {
                let resource = new OceanaCustomerWebVoiceVideo.Services.Work.Schema.Resource();
                resource.setNativeResourceId(workRequest.nativeResourceId);
                resource.setSourceName(workRequest.sourceName);

                work.setResources([resource]);
            }

        } else {
            console.log('SDK: invalid service or resource object provided');
        }
    },

    getToken: function (audioInteractionCallback) {
        const data = {
            "use": "csaGuest",
            "calledNumer": optional ? optional.destinationAddress : [],
            "callingNumber": customer ? customer.fromAddress : [],
            "displayName": customer ? customer.displayName : []
        }
        
        $.ajax({
        	url: `${tokenServiceConfig.secure ? "https" : "http"}://${tokenServiceConfig.tokenServiceAddress}/${tokenServiceConfig.urlPath}`,
        	method: "post",
        	data: JSON.stringify(data),
        	//contentType: "application/vnd.avaya.csa.tokens.v1+json",
        	dataType: 'json',
            timeout: 5000
        }).done(function(response){
        	const token = data.encryptedToken;
            localStorage.setItem('AVAYA.CLIENT.authToken', token);
            audioInteractionCallback();
        }).fail(function(error){
        	console.error('Não foi possível capturar token: ' + error.statusText);
        });
    },

    createInteraction: function () {
        if (work) {
            work.setContext(optional.context);
            work.setTopic(optional.topic);

            const plataform = localStorage.getItem('AVAYA.CLIENT.platform');
            audioInteraction = work.createAudioInteraction(plataform);

            console.log('audioInteraction: ' + JSON.stringify(audioInteraction));

            if (audioInteraction) {
                const auth = localStorage.getItem('AVAYA.CLIENT.authToken');
                audioInteraction.setAuthorizationToken(auth);
                
                audioInteraction.addOnAudioInteractionInitiatingCallback(() => {
                    active = true;
                    console.info('Reference Client: INITIATING!!!');
                    console.log(audioInteraction.getInteractionState());

                    let state = AVAYAFUNCTIONS.convertInteractionState(audioInteraction.getInteractionState());
                    console.log("state: " + JSON.stringify(state));
                });
                
                audioInteraction.addOnAudioInteractionRemoteAlertingCallback(() => {
                	console.info('Reference Client: REMOTE_ALERTING!!!');
                	let state = AVAYAFUNCTIONS.convertInteractionState(audioInteraction.getInteractionState());
                	
                	//Evento de intervalo de tempo
                	iniciarTimer();
                });
                
                audioInteraction.addOnAudioInteractionCallQualityCallback((callQuality) => {
			      console.info('Reference Client: Call quality!!!!!!!!  ' + callQuality);
			    });
			    
			    audioInteraction.addOnAudioInteractionActiveCallback(() => {
		          console.info('Reference Client: ESTABLISHED!!!');
		          let state = AVAYAFUNCTIONS.convertInteractionState(audioInteraction.getInteractionState());
		          console.log("state: " + JSON.stringify(state));
		        });
		        
		        audioInteraction.addOnAudioInteractionEndedCallback(() => {
		          active = false;
		          console.info('Reference Client: INTERACTION_ENDED!!!');
		          let state = AVAYAFUNCTIONS.convertInteractionState(audioInteraction.getInteractionState());
		          console.log("State Ended: "+JSON.stringify(state));
		        });
		        
		        audioInteraction.addOnAudioInteractionFailedCallback((data) => {
		          active = false;
		          console.info('Reference Client: INTERACTION_FAILED!!!');
		          console.error('error: ' + data.reason);
		          let state = AVAYAFUNCTIONS.convertInteractionState(audioInteraction.getInteractionState());
		          console.log("State Failed: "+JSON.stringify(state));
		        });
		        
		        audioInteraction.addOnAudioInteractionAudioMuteStatusChangedCallback((state) => {
		        
		          console.info('Reference Client: Audio Mute state change [' + state + ']');
		          
		          audioInteraction.addOnAudioInteractionHoldStatusChangedCallback((state) => {
		            console.info('Reference Client: Hold state change [' + state + ']');
		            state = AVAYAFUNCTIONS.convertInteractionState(audioInteraction.getInteractionState());
		          });
		
		          audioInteraction.addOnAudioInteractionServiceConnectedCallback(() => {
		            console.log('Reference Client: AUDIO SERVICE [connected]');
		          });
		
		          audioInteraction.addOnAudioInteractionServiceConnectingCallback(() => {
		            console.log('Reference Client: AUDIO SERVICE [connecting]');
		          });
		
		          audioInteraction.addOnAudioInteractionServiceDisconnectedCallback(() => {
		            console.log('Reference Client: AUDIO SERVICE [disconnected]');
		          });
		        });

                var destination = optional.destinationAddress;

                if (destination) {
                    audioInteraction.setDestinationAddress(destination);
                }
                
                audioInteraction.setContextId(optional.context);
                audioInteraction.setPlatformType(plataform);
            }
        } else {
            console.log('Reference Client: no work instance exists please create a work instance with createWork()');
        }
    },

    loadConfiguration: function () {
        let aawgString = localStorage.getItem('AVAYA.CLIENT.aawg.config');
        if (aawgString) {
            aawgConfig = JSON.parse(aawgString);
        }

        let customerString = localStorage.getItem('AVAYA.CLIENT.customer');
        if (customerString) {
            customer = JSON.parse(customerString);
        }

        let optionalString = localStorage.getItem('AVAYA.CLIENT.optional');
        if (optionalString) {
            optional = JSON.parse(optionalString);
        }

        let tokenServiceString = localStorage.getItem('AVAYA.CLIENT.token.config');
        if (tokenServiceString) {
            tokenServiceConfig = JSON.parse(tokenServiceString);
        }

        let workRequestString = localStorage.getItem('AVAYA.CLIENT.workRequest');
        if (workRequestString) {
            workRequest = JSON.parse(workRequestString);
        }
    },
    toggleMuteAudio: function () {
    	console.info('Reference Client: toggling audio mute');
        audioInteraction.muteAudio(!audioInteraction.isAudioMuted());
        let icon = audioInteraction.isAudioMuted() ? "ico-v4-nav-vazio" : "ico-v4-nav-audio";
        $(".btn-mute-voip i").attr("class", icon);
    },
    endCall: function () {
    	$.when(audioInteraction.end()).done(AVAYAFUNCTIONS.encerrarTimer).then(function(){
    		console.log("Fim da chamada");
    	});
    },
    _pad(unit) {
        return ('00' + unit).slice(-2);
    },
    formatCallTime : function(callTimeElapsed) {

        if(isNaN(callTimeElapsed)) {
          return [];
        }

        const callTimeSeconds = Math.floor(callTimeElapsed / 1000);
        const seconds = callTimeSeconds % 60;
        const minutes = (callTimeSeconds - seconds) / 60;
    
        return AVAYAFUNCTIONS._pad(minutes) + ":" + AVAYAFUNCTIONS._pad(seconds);
    },
    convertInteractionState: function(state) {
        let InteractionState = OceanaCustomerWebVoiceVideo.Services.Work.InteractionState;
    
        var keys = Object.keys(InteractionState).reduce(function (acc, key) {
          return acc[InteractionState[key]] = key, acc;
        }, {});
    
        return keys[state];
    },
    iniciarTimer: function(){
    	timerCall = setInterval(AVAYAFUNCTIONS.atualizarTimer, 1000);
    },
    atualizarTimer: function(){
    	//timerPassed = AVAYAFUNCTIONS.formatCallTime((audioInteraction.getInteractionTimeElapsed()));
    	timerPassed = timerPassed + 1000;
    	$(".timer-voip__container").text(AVAYAFUNCTIONS.formatCallTime(timerPassed));
    },
    encerrarTimer: function(){
    	clearInterval(timerCall);
    	console.log("Timer finalizado");
    }
}

$(document).ready(function () {
    AVAYAFUNCTIONS.loadConfiguration();
    AVAYAFUNCTIONS.start();
});