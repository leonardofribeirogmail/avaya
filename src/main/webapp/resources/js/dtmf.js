/*! (C) 2021 VENEA.NET */
var dtmf = {
    play: false,
    ready: false,
    keys: null,
    audio: null,
    tone0: null,
    tone1: null,
    sequence: null,
    time_on: -1,
    time_off: -1,
    space: -1,
    duration: -1,
    button_replay: null,
    button_restop: null,
    restop_flag: false,
    init: function() {
        dtmf.audio = new window.AudioContext();
        dtmf.ready = true;
    },
    freqs: function(key) {
        let freqs = dtmf.keys[key];
        if (freqs !== undefined) {
            return freqs;
        }
        return null;
    },
    start: function(event = null, replay = null) {
        if (!dtmf.ready) {
            dtmf.init();
        }
        if (dtmf.play) {
            return false;
        }
        dtmf.duration = 80;
        dtmf.space = 25;
        let key = -1;
        if (replay !== null) {
            key = replay;
        } else if (event instanceof MouseEvent) {
            key = this.innerText.trim();
            dtmf.sequence.value += "" + dtmf.extrair(key);
        } else {
            key = event.key;
        }

        key = dtmf.extrair(key);

        let tones = dtmf.freqs(key);
        if (tones === null) {
            return false;
        }
        dtmf.play = true;
        dtmf.tone0 = dtmf.audio.createOscillator();
        dtmf.tone0.type = 'sine';
        dtmf.tone0.frequency.setValueAtTime(tones[0], dtmf.audio.currentTime);
        dtmf.tone0.connect(dtmf.audio.destination);
        dtmf.tone0.start();
        dtmf.tone1 = dtmf.audio.createOscillator();
        dtmf.tone1.type = 'sine';
        dtmf.tone1.frequency.setValueAtTime(tones[1], dtmf.audio.currentTime);
        dtmf.tone1.connect(dtmf.audio.destination);
        dtmf.tone1.start();
        dtmf.time_on = new Date().getTime();
        return true;
    },
    stop: function() {
        dtmf.time_off = new Date().getTime();
        let ms = dtmf.time_off - dtmf.time_on;
        if (ms < dtmf.duration) {
            setTimeout(dtmf.stop, dtmf.duration + 1 - ms);
            return;
        }
        if (dtmf.tone0 !== null) {
            dtmf.tone0.stop();
            dtmf.tone0 = null;
        }
        if (dtmf.tone1 !== null) {
            dtmf.tone1.stop();
            dtmf.tone1 = null;
        }
        dtmf.play = false;
    },
    restop: function() {
        dtmf.restop_flag = true;
    },
    backspace: function() {
        dtmf.sequence.value = dtmf.sequence.value.substring(0, dtmf.sequence.value.length - 1);
    },
    clear: function() {
        document.getElementById('dtmf_sequence').value = '';
    },
    replay: function(index = -1, space = false) {
        if (index !== -1) {
            dtmf.stop();
        } else {
            dtmf.button_replay.disabled = true;
            dtmf.keyboard(false);
            dtmf.button_restop.disabled = false;
        }
        if (space) {
            setTimeout(dtmf.replay, dtmf.space, index, false);
            return;
        }
        if (!dtmf.restop_flag && index < dtmf.sequence.value.length) {
            dtmf.sequence.selectionStart = index + 1;
            dtmf.sequence.selectionEnd = index + 2;
            dtmf.sequence.focus();
            index++;
            key = dtmf.sequence.value.charAt(index);
            dtmf.start(null, key);
            setTimeout(dtmf.replay, dtmf.duration, index, true);
        } else {
            dtmf.button_replay.disabled = false;
            dtmf.keyboard(true);
            dtmf.button_restop.disabled = true;
            dtmf.restop_flag = false;
            dtmf.sequence.focus();
        }
    },
    keyboard: function(enable) {
        buttons = document.querySelectorAll('.dialpad button');
        buttons.forEach(function(button, index) {
            button.disabled = !enable;
        });
    },
    load: function() {
        dtmf.sequence = document.getElementById('voip-dial-number');
        dtmf.button_replay = document.getElementById('dtmf_replay');
        dtmf.button_restop = document.getElementById('dtmf_restop');
        let k = new Array(16);
        let f = [1209, 1336, 1477, 1633, 697, 770, 852, 941];
        k['1'] = [f[0], f[4]];
        k['2'] = [f[1], f[4]];
        k['3'] = [f[2], f[4]];
        k['4'] = [f[0], f[5]];
        k['5'] = [f[1], f[5]];
        k['6'] = [f[2], f[5]];
        k['7'] = [f[0], f[6]];
        k['8'] = [f[1], f[6]];
        k['9'] = [f[2], f[6]];
        k['*'] = [f[0], f[7]];
        k['0'] = [f[1], f[7]];
        k['#'] = [f[2], f[7]];
        dtmf.keys = k;
        buttons = document.querySelectorAll('.dialpad button');
        buttons.forEach(function(button, index) {
            button.addEventListener('mousedown', dtmf.start);
            button.addEventListener('mouseup', dtmf.stop);
            button.addEventListener('mouseleave', dtmf.stop);
        });
    },
	extrair: function(key){
		key = key.trim();
		if(key === "#" || key === "*") {			
        	key = key.toUpperCase()
		} else {
			key = key.toUpperCase().replace(/\D/g, "");
		}
		return key;
	}
};
document.addEventListener('DOMContentLoaded', dtmf.load);